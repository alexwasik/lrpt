require 'sinatra'
require 'sinatra/reloader' if development?
require 'csv'
require 'pry'

get '/' do
  erb :index
end

post '/parse' do
  @grouped_banks = []
  @no_rev = []
  banks = params[:banks].split(',')
  @csv =  CSV.parse(params[:file][:tempfile], headers: true, header_converters: :symbol)

  @csv.each do |row|
    # create array of included banks
    if banks.include? row[:bank]
      @grouped_banks << row
    end
  end
  # remove line if prev_pay = 1 
  @main_arr = []
  @grouped_banks.each do |row|
    @main_arr << row unless row[:pay_rev] == "1"
  end
  
  #sort by 'G'
  @main_arr.sort_by! { |e| [e[6]] }.reverse

  erb :parse
end