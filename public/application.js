//Vue is the best

new Vue({
  el: '#app', 
  data() {
    return {
      hello: 'Hello',
      loading: false
    }
  },
  methods: {
    generateData: function() {
      this.loading = true;
    }
  },
  created: {
    this.loading = true;
    this.hello = 'goodbye'
  }
})